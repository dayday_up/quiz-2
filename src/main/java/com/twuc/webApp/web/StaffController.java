package com.twuc.webApp.web;

import com.twuc.webApp.domain.TimeZoneId;
import com.twuc.webApp.models.entity.StaffEntity;
import com.twuc.webApp.models.entityRepository.StaffEntityRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class StaffController {
    private final StaffEntityRepository staffEntityRepository;

    public StaffController(StaffEntityRepository staffEntityRepository) {
        this.staffEntityRepository = staffEntityRepository;
    }

    @PostMapping("/staffs")
    public ResponseEntity createStaff(@RequestBody StaffEntity staff) {
        StaffEntity savedStaff = staffEntityRepository.save(staff);

        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Location", "/api/staffs/" + savedStaff.getId())
                .build();
    }

    @GetMapping("/staffs/{staffId}")
    public ResponseEntity findStaff(@PathVariable Long staffId) {
        Optional<StaffEntity> staff = staffEntityRepository.findById(staffId);

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(staff);
    }

    @GetMapping("/staffs")
    public ResponseEntity findAllStaff() {
        List<StaffEntity> allStaff = staffEntityRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(allStaff);
    }

    @PutMapping("/staffs/{staffId}/timezone")
    public ResponseEntity addTimeZoneForStaff(@PathVariable Long staffId, @RequestBody TimeZoneId timeZone) {
        StaffEntity staff = staffEntityRepository.findById(staffId).get();
        staff.setZoneId(timeZone.getZoneId());
        staffEntityRepository.save(staff);
        return ResponseEntity.status(HttpStatus.OK)
                .build();
    }

    @GetMapping("/timezones")
    public ResponseEntity getTimeZones() {
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(ZoneRulesProvider.getAvailableZoneIds());
    }
}
