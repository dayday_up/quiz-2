package com.twuc.webApp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.domain.TimeZoneId;
import com.twuc.webApp.models.entity.StaffEntity;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.sql.Time;
import java.time.zone.ZoneRulesProvider;
import java.util.Set;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class StaffControllerTest extends ApiTestBase {

    private ResultActions createStaff(String firstName, String lastName) throws Exception {
        StaffEntity staff = new StaffEntity(firstName, lastName);
        String staffJson = mapper.writeValueAsString(staff);
        return mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(staffJson));
    }

    private ResultActions addTomeZoneForStaff(String location, String timeZone) throws Exception {
        return mockMvc.perform(put(location)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(new TimeZoneId(timeZone))));
    }

    private String createTimeZoneId() {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        return availableZoneIds.iterator().next();
    }



    @Test
    void should_return_201_and_location() throws Exception {
        createStaff("Stephen", "curry")
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1"));
    }

    @Test
    void should_return_staff_when_get_staff() throws Exception {
        MvcResult response = createStaff("Stephen", "curry").andReturn();
        String location = response.getResponse().getHeader("Location");

        mockMvc.perform(get(location)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Stephen"))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.lastName").value("curry"));
    }

    @Test
    void should_return_all_staff() throws Exception {
        createStaff("Stephen", "curry");
        createStaff("Lebron", "James");
        createStaff("Yuntian", "Wen");

        mockMvc.perform(get("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));

    }

    @Test
    void should_return_empty_list_when_have_no_staff() throws Exception {
        mockMvc.perform(get("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));

    }

    @Test
    void should_return_time_zone_when_added() throws Exception {
        MvcResult response = createStaff("Stephen", "curry").andReturn();
        String location = response.getResponse().getHeader("Location");
        String timeZoneId = createTimeZoneId();
        addTomeZoneForStaff(location + "/timezone", timeZoneId)
                .andExpect(status().isOk());
    }

    @Test
    void should_return_staff_with_time_zone_id_when_added() throws Exception {
        MvcResult response = createStaff("Stephen", "curry").andReturn();
        String location = response.getResponse().getHeader("Location");
        String timeZoneId = createTimeZoneId();
        addTomeZoneForStaff(location + "/timezone", timeZoneId);

        mockMvc.perform(get(location)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.zoneId").value(timeZoneId))
                .andExpect(jsonPath("$.firstName").value("Stephen"))
                .andExpect(jsonPath("$.lastName").value("curry"));
    }

    @Test
    void should_time_zone_list() throws Exception {
        mockMvc.perform(get("/api/timezones")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()));
    }
}
