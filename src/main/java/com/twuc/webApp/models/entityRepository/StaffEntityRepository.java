package com.twuc.webApp.models.entityRepository;

import com.twuc.webApp.models.entity.StaffEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StaffEntityRepository extends JpaRepository<StaffEntity, Long> {
    public List<StaffEntity> findAllByOrderByIdAsc();
}
