package com.twuc.webApp.domain;

import javax.validation.constraints.NotNull;

public class TimeZoneId {
    @NotNull
    private String zoneId;

    public TimeZoneId() {

    }

    public TimeZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }
}
